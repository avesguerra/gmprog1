﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suikoden_Fin
{
    class Program
    { 

    static void Main(string[] args)
        {

            int hpPlayer;
            int minDmgPlayer;
            int maxDmgPlayer;
            int atkPlayer;
            int hpPlayerNew;

            int hpOpp;
            int minDmgOpp;
            int maxDmgOpp;
            int atkOpp;
            int hpOppNew;

            int oppMove;

            //int choice;
            //player
            Console.Write("Please enter HP of Player: ");
            hpPlayer = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Minimum Dmg of Player: ");
            minDmgPlayer = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Maximum Dmg of Player: ");
            maxDmgPlayer = Convert.ToInt32(Console.ReadLine());

            //opponent
            Console.Write("Please enter HP of Opponent: ");
            hpOpp = Convert.ToInt32(Console.ReadLine());
                        
            Console.Write("Please enter Minimum Dmg of Opponent: ");
            minDmgOpp = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Maximum Dmg of Opponent: ");
            maxDmgOpp = Convert.ToInt32(Console.ReadLine());

            //values
            //player
            Console.WriteLine("Player's Hp: {0}", hpPlayer);
            Console.WriteLine("Player's Minimum Dmg: {0}", minDmgPlayer);
            Console.WriteLine("Player's Maximum Dmg: {0}", maxDmgPlayer);

            Console.WriteLine("Opponent's Hp: {0}", hpOpp);
            Console.WriteLine("Opponent's Minimum Dmg: {0}", minDmgOpp);
            Console.WriteLine("Opponent's Maximum Dmg: {0}", maxDmgOpp);

            Console.WriteLine("Press any key to continue....");
            Console.ReadLine();


            int choice;
            atkOpp = RandomHelper.Range(minDmgOpp, maxDmgOpp);
            atkPlayer = RandomHelper.Range(minDmgPlayer, maxDmgPlayer);

            //hpOppNew = atkPlayer - hpOpp;
            //hpPlayerNew = atkOpp - hpPlayer;

            //if (hpPlayerNew > hpPlayer) hpPlayerNew = hpPlayer;

            //if (hpOppNew > hpOpp) hpOppNew = hpOpp;


            //while (hpPlayer <= 0 || hpOpp <= 0)
            //{
            //    Console.WriteLine("End Game");
            //    Console.ReadLine();
            //}
            hpOppNew = hpOpp;
            hpPlayerNew = hpPlayer;
            while (hpPlayer >= 1 && hpOpp >= 1)
            {
                Console.WriteLine("Please choose one");
                Console.WriteLine("1 - Attack");
                Console.WriteLine("2 - Defend");
                Console.WriteLine("3 - Wild Attack");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    //----------------------------------------------//
                    case 1: //Attack

                        atkPlayer = RandomHelper.Range(minDmgPlayer, maxDmgPlayer);
                        hpOpp = hpOpp - atkPlayer;

                        Console.WriteLine("Player {0}", hpPlayerNew);
                        Console.WriteLine("Opponent {0}", hpOpp);

                        if (hpOpp >= 0)
                        {
                            Console.WriteLine("You won!");
                            Console.ReadLine();
                            return;
                        }
                        else if (hpPlayer <= 0)
                        {
                            Console.WriteLine("You lost..");
                            Console.ReadLine();
                            return;
                        }
                        
                        break;
                    //----------------------------------------------//
                    case 2: //Defend
                        Console.WriteLine("Player {0}", hpPlayerNew);
                        Console.WriteLine("Opponent {0}", hpOppNew);
                        Console.ReadLine();

                        if (hpOppNew <= 0)
                        {
                            Console.WriteLine("You won!");
                            Console.ReadLine();
                            return;
                        }
                        else if (hpPlayerNew <= 0)
                        {
                            Console.WriteLine("You lost..");
                            Console.ReadLine();
                            return;
                        }

                        break;
                    //----------------------------------------------//
                    case 3: //Wild Attack
                        atkPlayer = RandomHelper.Range(minDmgPlayer, maxDmgPlayer);
                        hpOpp = hpOpp - atkPlayer;


                        Console.WriteLine("Player {0}", hpPlayerNew);
                        Console.WriteLine("Opponent {0}", hpOppNew);


                        if (hpOppNew <= 0)
                        {
                            Console.WriteLine("You won!");
                            Console.ReadLine();
                            return;
                        }
                        else if (hpPlayerNew <= 0)
                        {
                            Console.WriteLine("You lost..");
                            Console.ReadLine();
                            return;
                        }

                        break;
                        //----------------------------------------------//
                }

                

            }

                Console.ReadLine();
                return;

        }
    }
}
